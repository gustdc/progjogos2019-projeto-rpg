
local ListMenu = require 'view.list_menu'
local State = require 'state'

local EquipmentSelectionState = require 'common.class' (State)

function EquipmentSelectionState:_init(stack)
  self:super(stack)
  self.party = nil
  self.equipment = nil
  self.menu = nil
end

function EquipmentSelectionState:enter(params)
  self.party = params.party
  self.equipment = params.equipment

  local equip_names = {}
  for i, equip in ipairs(self.equipment) do
      equip_names[i] = equip:get_name()
  end

  self.menu = ListMenu(equip_names)
  self:_show_menu()
end

function EquipmentSelectionState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 20, (bfbox.top + bfbox.bottom) / 2 + 15)
  self:view():add('menu', self.menu)

  local message = "Choose an equipment!"
  message = message .. "\n" .. "<Press ESC to return to menu>"
  self:view():get("message"):set(message)
end

function EquipmentSelectionState:resume(params)
  if params.back then
    self:_show_menu()
  elseif params.selected then
    local character = params.selected[1]
    local selected_equip = self.equipment[self.menu:current_option()]
    local message = character:equip(selected_equip)
    if message == "" then
      self:pop({
        equipped = true,
        character = character,
        equipment = selected_equip
      })
    else
      self:pop({
        equipped = false,
        message = message
      })
    end

  end
end

function EquipmentSelectionState:leave()
  self:view():remove('menu')
end

function EquipmentSelectionState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    self.menu:submit_option()
    self:push("target_selection", { targets = self.party })
  elseif key == 'escape' then
    self:pop({ back = true })
  end
end

return EquipmentSelectionState
