local Vec = require 'common.vec'
local State = require 'state'
local filter = require 'common.filter'

local EncounterState = require 'common.class' (State)

local CHARACTER_GAP = 96

local MESSAGES = {
  Begin = "You stumble upon an encounter",
  Fight = "%s attacked %s for %i damage!",
  Skill = "%s unleashed %s on ",
  Item = "%s used %s for %i %s",
  Win  = "You won the battle!",
  Lose = "You lost the battle!",
  Struck = "%i HP remains.",
  Dead = "%s is dead!",
  Prompt = "<Press ENTER to continue>"
}

function EncounterState:_init(stack)
  self:super(stack)
  self.turns = nil
  self.next_turn = nil
  self.players = nil
  self.backpack = nil
  self.enemies = nil
end

function EncounterState:enter(params)
  local atlas = self:view():get('atlas')
  local battlefield = self:view():get("battlefield")
  local message = self:view():get("message")
  local party_origin = battlefield:east_team_origin()
  self.turns = {}
  self.players = {}
  self.backpack = params.backpack
  self.next_turn = 1
  local n = 0
  self.lost_encounter = false
  self.won_encounter = false

  for i, character in ipairs(params.party) do
    local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[i] = character
    self.players[i] = character
    atlas:add(character, pos, character:get_appearance())
    n = n + 1
  end

  local encounter_origin = battlefield:west_team_origin()
  self.enemies = {}
  for i, character in ipairs(params.encounter) do
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[n + i] = character
    self.enemies[i] = character
    atlas:add(character, pos, character:get_appearance())
  end

  table.sort(self.turns, function(a, b) return a.speed > b.speed end)

  message:set(MESSAGES["Begin"] .. "\n" .. MESSAGES["Prompt"])
  self.waiting_player_prompt = true
end

function EncounterState:on_keypressed(key)
  if key == 'return' then
    self.waiting_player_prompt = false
    if not (self.won_encounter or self.lost_encounter) then
      self:check_encounter_status()
    end
  end
end

function EncounterState:update(_)
  if not self.waiting_player_prompt then
    local curr_char_is_enemy = true

    local current_character = self.turns[self.next_turn]
    local params = {current_character = current_character,
                    players = self.players,
                    enemies = self.enemies,
                    backpack = self.backpack}

    for _, k in ipairs(self.players) do
      if current_character == k then
        curr_char_is_enemy = false
        break
      end
    end

    if self.won_encounter then
      self:pop({win = true, party = self.players, backpack = self.backpack})
    elseif self.lost_encounter then
      self:pop({win = false, party = self.players, backpack = self.backpack})
    else
      self.next_turn = self.next_turn % #self.turns + 1
      if curr_char_is_enemy then
        return self:push('enemy_turn', params)
      else
        return self:push('player_turn', params)
      end
    end
  end
end

function EncounterState:resume(params)
  if params.action == 'Run' then
    return self:pop({win = true, party = self.players, backpack = self.backpack})
  elseif params.action == "Fight" then
    self:issue_attack(params.character, params.target)
  elseif params.action == "Skill" then
    local t
    if params.skill.type == "heal" then
      t = self.players
    else
      t = params.targets
    end
    self:issue_skill(params.skill, params.character, t)
  elseif params.action == "Item" then
    self:issue_item(params.character, params.item)
  end
  self.waiting_player_prompt = true
end

function EncounterState:issue_attack(attacker, defender)
  local damage = attacker:attack(defender)

  local message = MESSAGES["Fight"]:format(attacker:get_name(), defender:get_name(), damage)
  if defender:is_dead() then
    message = message .. "\n" .. MESSAGES["Dead"]:format(defender:get_name())
  else
    message = message .. "\n" .. MESSAGES["Struck"]:format(defender:get_hp())
  end
  message = message .. "\n" .. MESSAGES["Prompt"]

  self:view():get("message"):set(message)
end

function EncounterState:issue_skill(skill, attacker, targets)
  local effect = skill:use_skill(attacker, targets)
  local message

  if type(effect) ~= "string" then
    message = MESSAGES["Skill"]:format(attacker:get_name(), skill:get_name()) .. '\n'
    local sign = "+"
    for i,_ in ipairs(targets) do
      if skill.type == "damage_amplifier" then
        sign = "-"
      end
      message = message .. targets[i]:get_name() .. ' (' .. sign .. effect[i] .. ')'
      if i == #targets then
        message = message .. ','
      end
      message = message .. ' '
      end
  else
    message = effect
  end

  message = message .. "\n" .. MESSAGES["Prompt"]
  self:view():get("message"):set(message)
end

function EncounterState:issue_item(character, item)
  local effect, quantity = item:use_item(character)
  if quantity == 0 then
    for i, bkpkitem in ipairs(self.backpack) do
      if bkpkitem.name == item:get_name() then
        table.remove(self.backpack, i)
      end
    end
  end
  local message = MESSAGES["Item"]:format(
                                    character:get_name(),
                                    item:get_name(),
                                    effect,
                                    item:get_stat())
  message = message .. "\n" .. MESSAGES["Prompt"]
  self:view():get("message"):set(message)
end

function EncounterState:check_encounter_status()
  for k, player in ipairs(self.players) do
    if player:is_dead() then
      table.remove(self.players, k)
      self:view():get("atlas"):remove(player)
    end
  end

  for k, enemy in ipairs(self.enemies) do
    if enemy:is_dead() then
      table.remove(self.enemies, k)
      self:view():get("atlas"):remove(enemy)
    end
  end

  for k, unit in ipairs(self.turns) do
    if unit:is_dead() then
      table.remove(self.turns, k)
    end
  end

  self.turns = filter(self.turns, function(el) return el:get_hp() > 0 end)

  if self.next_turn > #self.turns then
    self.next_turn = self.next_turn - 1
  end

  if #self.enemies == 0 then
    self.won_encounter = true
    self.waiting_player_prompt = true
    self:view():get("message"):set(MESSAGES["Win"] .. "\n" .. MESSAGES["Prompt"])
  elseif #self.players == 0 then
    self.lost_encounter = true
    self.waiting_player_prompt = true
    self:view():get("message"):set(MESSAGES["Lose"] .. "\n" .. MESSAGES["Prompt"])
  end
end

return EncounterState
