local State = require "state"

local EnemyTurnState = require "common.class"(State)

function EnemyTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.target = nil
end

function EnemyTurnState:enter(params)
  self.character = params.current_character
  self.targets = params.players

  local BEHAVIOR_HANDLER = {
    attack_strongest = self.attack_strongest,
    attack_lowest_hp = self.attack_lowest_hp
  }

  local do_behavior = BEHAVIOR_HANDLER[self.character.behavior]

  if do_behavior ~= nil then
    do_behavior(self)
  else
    self:default_behavior()
  end
end

function EnemyTurnState:default_behavior()
  -- default behavior: select first alive party member
  for i,k in ipairs(self.targets) do
    if not k:is_dead() then
      self.target = self.targets[i]
      break
    end
  end

  self:pop({character = self.character, target = self.target, action = "Fight"})
end

function EnemyTurnState:attack_strongest()
  self.target = self.targets[1]
  for _, enemy in ipairs(self.targets) do
    if enemy.str > self.target.str then
      self.target = enemy
    end
  end

  self:pop({character = self.character, target = self.target, action = "Fight"})
end

function EnemyTurnState:attack_lowest_hp()
  self.target = self.targets[1]
  for _, enemy in ipairs(self.targets) do
    if enemy:get_hp() < self.target:get_hp() then
      self.target = enemy
    end
  end

  self:pop({character = self.character, target = self.target, action = "Fight"})
end

return EnemyTurnState
