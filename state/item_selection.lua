local ListMenu = require 'view.list_menu'
local State = require 'state'

local ItemSelectionState = require 'common.class' (State)

function ItemSelectionState:_init(stack)
  self:super(stack)
  self.character = nil
  self.party = nil
  self.backpack = nil
  self.menu = nil
end

function ItemSelectionState:enter(params)
  self.character = params.current_character
  self.backpack = params.backpack
  local item_names = {}

  for i, item in ipairs(self.backpack) do
      item_names[i] = item:get_name() .. " x" .. item:get_quantity()
  end

  self.menu = ListMenu(item_names)
  self:_show_menu()

  local message = "Choose an item!"
  message = message .. "\n" .. "<Press ESC to return to menu>"
  self:view():get("message"):set(message)
end

function ItemSelectionState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 20, (bfbox.top + bfbox.bottom) / 2 + 15)
  self:view():add('menu', self.menu)
end

function ItemSelectionState:leave()
  self:view():remove('menu')
end

function ItemSelectionState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    self.menu:submit_option()
    self:pop({
      item = self.backpack[self.menu:current_option()]
    })
  elseif key == 'escape' then
    self:pop({back = true})
  end
end

return ItemSelectionState
