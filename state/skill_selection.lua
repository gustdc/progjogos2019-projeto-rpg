local ListMenu = require 'view.list_menu'
local State = require 'state'

local SkillSelectionState = require 'common.class' (State)

function SkillSelectionState:_init(stack)
  self:super(stack)
  self.character = nil
  self.party = nil
  self.enemies = nil
  self.menu = nil
end

function SkillSelectionState:enter(params)
  self.character = params.current_character
  self.party = params.party
  self.enemies = params.enemies

  local skill_list = {}

  for i, skill in ipairs(self.character.skills) do
    skill_list[i] = skill:get_name()
  end

  self.menu = ListMenu(skill_list)
  self:_show_menu()

  local message = "Choose a skill!"
  message = message .. "\n" .. "<Press ESC to return to menu>"
  self:view():get("message"):set(message)
end

function SkillSelectionState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 20, (bfbox.top + bfbox.bottom) / 2 + 15)
  self:view():add('menu', self.menu)
end

function SkillSelectionState:leave()
  self:view():remove('menu')
end

function SkillSelectionState:resume(params)
    params.skill = self.character.skills[self.menu:current_option()]
    self:pop(params)
end

function SkillSelectionState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    self.menu:submit_option()
    self.selected_skill = self.character.skills[self.menu:current_option()]
    local params = { current_character = self.character,
      amount = self.selected_skill.number_of_targets }

    if self.selected_skill:is_friendly() then
      params.targets = self.party
    else
      params.targets = self.enemies
    end

    if self.selected_skill.number_of_targets > 0 then
      self:push("target_selection", params)
    else
      self:pop({current_character = self.character, selected = self.enemies,
        skill = self.character.skills[self.menu:current_option()]})
    end
  elseif key == 'escape' then
    self:pop({back = true})
  end
end

return SkillSelectionState
