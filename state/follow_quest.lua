local Character = require 'model.character'
local Equipment = require 'model.equipment'
local Item = require 'model.item'
local State = require 'state'

local FollowQuestState = require 'common.class' (State)

function FollowQuestState:_init(stack)
  self:super(stack)
  self.party = nil
  self.backpack = nil
  self.encounters = nil
  self.next_encounter = nil
end

function FollowQuestState:enter(params)
  local quest = params.quest

  self.encounters = quest.encounters
  self.next_encounter = 1
  self.party = {}
  self.equipment = {}
  self.backpack = {}

  for i, character_name in ipairs(quest.party) do
    local character_spec = require('database.characters.' .. character_name)
    self.party[i] = Character(character_spec)
  end

  for i, equip in ipairs(quest.equipment) do
    local equip_spec = require('database.equipment.' .. equip)
    self.equipment[i] = Equipment(equip_spec)
  end

  for i, item in ipairs(quest.backpack) do
    local item_spec = require('database.items.' .. item[1])
    self.backpack[i] = Item(item_spec, item[2])
  end

end

function FollowQuestState:resume(params)
  if params.win ~= nil then
    self.backpack = params.backpack
    self.party = params.party
    if not params.win then
      self:pop()
    end
  end
end

function FollowQuestState:update(_)
  if self.next_encounter <= #self.encounters then
    local encounter = {}
    local encounter_specnames = self.encounters[self.next_encounter]
    self.next_encounter = self.next_encounter + 1
    for i, character_name in ipairs(encounter_specnames) do
      local character_spec = require('database.characters.' .. character_name)
      encounter[i] = Character(character_spec)
    end
    local params = {
      party = self.party,
      equipment = self.equipment,
      backpack = self.backpack,
      encounter = encounter
    }
    return self:push('preparation', params)
  else
    return self:pop()
  end
end

return FollowQuestState


