local Vec = require 'common.vec'
local SpriteAtlas = require 'view.sprite_atlas'
local ListMenu = require 'view.list_menu'
local BattleField = require 'view.battlefield'
local MessageBox = require 'view.message_box'
local State = require 'state'

local PreparationState = require 'common.class' (State)

local PREPARATION_OPTIONS = { 'Continue', 'Equipment' }
local PREPARATION_MESSAGES = {
  Start = "Select or change equipment. Or continue to the adventure...",
  Equip = "%s equipped %s for %i %s",
  Prompt = "<Press ENTER to continue>"
}

local CHARACTER_GAP = 96

function PreparationState:_init(stack)
  self:super(stack)
  self.party = nil
  self.equipment = nil
  self.backpack = nil
  self.encounter = nil
  self.selected_equip = nil
  self.menu = ListMenu(PREPARATION_OPTIONS)
end

function PreparationState:enter(params)
  self.party = params.party
  self.equipment = params.equipment
  self.backpack = params.backpack
  self.encounter = params.encounter

  local atlas = SpriteAtlas()
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 48))

  local origin = battlefield:center_origin()
  local party_width = (16 + CHARACTER_GAP) * #self.party - CHARACTER_GAP
  local offset_origin = origin - Vec(0.75, 0) * (party_width / 2)

  for i, character in ipairs(self.party) do
    local pos = offset_origin + Vec(1, 0) * CHARACTER_GAP * (i - 1)
    atlas:add(character, pos, character:get_appearance())
  end

  self:view():add('atlas', atlas)
  self:view():add('battlefield', battlefield)
  self:view():add('message', message)

  self:_show_menu()
  self:view():get("message"):set(PREPARATION_MESSAGES["Start"])
end

function PreparationState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 20, (bfbox.top + bfbox.bottom) / 2 + 15)
  self:view():add('menu', self.menu)
end

function PreparationState:resume(params)
  local message

  if params.win ~= nil then
    self:pop(params)
  elseif params.back then
    message = PREPARATION_MESSAGES["Start"]
    self:_show_menu()
    self:view():get("message"):set(message)
  elseif params.equipped ~= nil then
    if params.equipped then
      for i, equip in ipairs(self.equipment) do
        if equip.name == params.equipment.name then
          table.remove(self.equipment, i)
        end
      end
      message = PREPARATION_MESSAGES["Equip"]:format(
                                        params.character:get_name(),
                                        params.equipment:get_name(),
                                        params.equipment:get_effect(),
                                        params.equipment:get_stat())
      message = message .. "\n" .. PREPARATION_MESSAGES["Prompt"]
    else
      message = params.message
    end
    self:_show_menu()
    self:view():get("message"):set(message)
  end
end

function PreparationState:leave()
  self:view():remove('battlefield')
  self:view():remove('message')
  self:view():get('atlas'):clear()
  self:view():remove('atlas')
end

function PreparationState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    self.menu:submit_option()
    local option = PREPARATION_OPTIONS[self.menu:current_option()]
    self:push_option(option)
  end
end

function PreparationState:push_option(option)
  if option == "Equipment" then
    if #self.equipment > 0 then
      self:push("equipment_selection", {party = self.party, equipment = self.equipment})
    else
      self:view():get("message"):set("There are no more equipment!")
    end
  elseif option == "Continue" then
    local params = {
      party = self.party,
      equipment = self.equipment,
      backpack = self.backpack,
      encounter = self.encounter }
    self:view():remove('menu')
    return self:push('encounter', params)
  end
end

return PreparationState

