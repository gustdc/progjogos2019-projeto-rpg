local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local PlayerTurnState = require 'common.class' (State)

local TURN_OPTIONS = { 'Fight', 'Skill', 'Item', 'Run' }
local TURN_MESSAGES = {
  Start = "What should %s do?",
  ["No Skills"] = "%s has no skills to use.",
  ["No Items"] = "There's no items to use."
}

function PlayerTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.menu = ListMenu(TURN_OPTIONS)
end

function PlayerTurnState:enter(params)
  self.character = params.current_character
  self.party = params.players
  self.backpack = params.backpack
  self.enemies = params.enemies
  self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
  self:view():get("message"):set(TURN_MESSAGES["Start"]:format(self.character:get_name()))
end

function PlayerTurnState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 20, (bfbox.top + bfbox.bottom) / 2 + 15)
  self:view():add('menu', self.menu)
end

function PlayerTurnState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function PlayerTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function PlayerTurnState:leave()
  self:view():remove('menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function PlayerTurnState:resume(params)

  if params.back then
    self:_show_menu()
    self:_show_cursor()
    self:_show_stats()
    self:view():get("message"):set(TURN_MESSAGES["Start"]:format(self.character:get_name()))
  else

    local option = TURN_OPTIONS[self.menu:current_option()]

    if option == "Fight" then
      return self:pop({action = option, character = self.character, target = params.selected[1]})
    elseif option == "Skill" then
      return self:pop({action = option, character = self.character, targets = params.selected,
                       skill = params.skill})
    elseif option == "Item" then
      return self:pop({action = option, character = self.character, item = params.item})
    elseif option == "Run" then
      return self.pop({action = option})
    end

  end
end

function PlayerTurnState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    self.menu:submit_option()
    local option = TURN_OPTIONS[self.menu:current_option()]
    self:push_option(option)
  end
end

function PlayerTurnState:push_option(option)
  if option == "Fight" then
    self:push("target_selection",
    {current_character = self.character, targets = self.enemies})
  elseif option == "Skill" then
    if self.character:has_skills() then
      self:push("skill_selection",
      {current_character = self.character, party = self.party, enemies = self.enemies})
    else
      self:view():get("message"):set(TURN_MESSAGES["No Skills"]:format(self.character:get_name()))
    end
  elseif option == "Item" then
    if #self.backpack > 0 then
      self:push("item_selection",
      {current_character = self.character, party = self.party, backpack = self.backpack})
    else
      self:view():get("message"):set(TURN_MESSAGES["No Items"])
    end
  else
    return self:pop({action = option, character = self.character})
  end
end

return PlayerTurnState

