local Vec = require 'common.vec'
local TurnCursor = require "view.turn_cursor"
local TargetStats = require "view.target_stats"
local ListMenu = require "view.list_menu"
local State = require "state"

local TargetSelectionState = require "common.class"(State)

function TargetSelectionState:_init(stack)
    self:super(stack)
    self.character = nil
    self.menu = nil
    self.amount = nil
    self.selected_targets = {}
end

function TargetSelectionState:enter(params)
    self.character = params.current_character
    self.targets = params.targets
    self.amount = params.target_amount or 1
    self.selected_targets = {}
    local menu_items = {}

    for i, target in ipairs(self.targets) do
        menu_items[i] = target:get_name()
    end

    self.menu = ListMenu(menu_items)

    self:_show_menu()
    self:_show_cursor()
    self:_show_stats()

  local message = "Choose a target!"
  message = message .. "\n" .. "<Press ESC to return to menu>"
  self:view():get("message"):set(message)
end

function TargetSelectionState:_show_menu()
    local bfbox = self:view():get("battlefield").bounds
    self.menu:reset_cursor()
    self.menu.position:set(bfbox.right + 20, (bfbox.top + bfbox.bottom) / 2 + 15)
    self:view():add("menu", self.menu)
end

function TargetSelectionState:_show_cursor()
    local atlas = self:view():get("atlas")
    local target_sprite = atlas:get(self.targets[self.menu:current_option()])
    local target_cursor = TurnCursor(target_sprite, "red")
    self:view():add("target_cursor", target_cursor)
end

function TargetSelectionState:_show_stats()
    local bfbox = self:view():get("battlefield").bounds
    local target = self.targets[self.menu:current_option()]
    local target_stats = TargetStats(Vec(bfbox.left, bfbox.bottom + 4), target)
    self:view():add("target_stats", target_stats)
end

function TargetSelectionState:leave()
    self:view():remove("menu")
    self:view():remove("target_cursor")
    self:view():remove("target_stats")
end

function TargetSelectionState:on_keypressed(key)
    if key == "down" then
        self.menu:next()
        self:_show_cursor()
        self:_show_stats()
    elseif key == "up" then
        self.menu:previous()
        self:_show_cursor()
        self:_show_stats()
    elseif key == "return" then
        self.menu:submit_option()
        self.amount = self.amount - 1
        table.insert(self.selected_targets, self.targets[self.menu:current_option()])
        if self.amount == 0 then
            self:pop({selected = self.selected_targets})
        else
            self:view():get("message"):set("Choose another target!")
        end
    elseif key == "escape" then
        self:pop({back = true})
    end

end

return TargetSelectionState
