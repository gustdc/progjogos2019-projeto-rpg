
local Vec = require 'common.vec'
local TurnCursor = require 'common.class' ()

function TurnCursor:_init(selected_drawable, color)
  self.offset = Vec(0, -48)
  self.selected_drawable = selected_drawable
  if color == "red" then
    self.r = 255
    self.g = 0
    self.b = 0
  else
    self.r = 1
    self.g = 1
    self.b = 1
  end
end

function TurnCursor:draw()
  if self.selected_drawable then
    local g = love.graphics
    local position = self.selected_drawable.position + self.offset
    g.push()
    g.translate(position:get())
    g.setColor(self.r, self.g, self.b)
    g.polygon('fill', -8, 0, 8, 0, 0, 8)
    g.pop()
  end
end

return TurnCursor

