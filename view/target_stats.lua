
local TargetStats = require 'common.class' ()

local STATS_MESSAGES = {
  Name = "%s | ",
  HP = "HP: %d/%d ",
  Res = "Res: %d/%d ",
  Stats = "Str: %d Def: %d Speed: %d ",
}

function TargetStats:_init(position, character)
  self.position = position
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 20)
  self.font:setFilter('nearest', 'nearest')
  self.equipment, self.max_equipment = character:get_equipment()

  self.name = STATS_MESSAGES["Name"]:format(character:get_name())
  self.hp = STATS_MESSAGES["HP"]:format(character:get_hp())
  self.res = STATS_MESSAGES["Res"]:format(character:get_res())
  self.stats = STATS_MESSAGES["Stats"]:format(
                                    character.str,
                                    character.def,
                                    character.speed)
end

function TargetStats:draw()
  local g = love.graphics
  g.push()
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.translate(self.position:get())
  g.print(self.name .. self.hp .. self.res .. self.stats)
  g.translate(0, self.font:getHeight())
  local equipstring = ""
  if #self.equipment > 0 then
    for i, equip in ipairs(self.equipment) do
      if i > 1 then
        equipstring = equipstring .. ", "
      end
      equipstring = equipstring .. equip.name .. " (" .. equip.type .. ")"
    end
  else
    equipstring = equipstring .. "nothing"
  end
  equipstring = equipstring .. " (MAX: " .. self.max_equipment .. ")"
  g.print("Equipping: " .. equipstring)
  g.pop()
end

return TargetStats

