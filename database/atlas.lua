
return {
  texture = 'kenney-1bit.png',
  frame_width = 16,
  frame_height = 16,
  gap_width = 1,
  gap_height = 1,
  sprites = {
    invalid = {
      frame = { 24, 25 },
      color = 'red'
    },
    slime = {
      frame = { 27, 8 },
      color = 'green'
    },
    blue_slime = {
      frame = { 27, 8 },
      color = 'blue'
    },
    red_slime = {
      frame = { 27, 8 },
      color = 'red'
    },
    knight = {
      frame = { 28, 0 },
      color = 'blue'
    },
    archer = {
      frame = { 32, 0 },
      color = 'red'
    },
    priest = {
      frame = { 24, 0 },
      color = 'white'
    }
  }
}

