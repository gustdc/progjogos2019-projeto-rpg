
return {
  name = 'Health Potion',
  appearance = 'health_potion',
  stat = 'hp',
  effect = 5
}
