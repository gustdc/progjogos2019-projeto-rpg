
return {
  name = 'Strength Potion',
  appearance = 'strength_potion',
  stat = 'str',
  effect = 3
}
