
return {
  name = "Pope's Hat",
  appearance = 'popes_hat',
  type = 'armor',
  stat = 'def',
  effect = 3
}
