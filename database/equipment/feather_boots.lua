
return {
  name = 'Feather Boots',
  appearance = 'feather_boots',
  type = 'artifact',
  stat = 'speed',
  effect = 2
}
