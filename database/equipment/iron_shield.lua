
return {
  name = 'Iron Shield',
  appearance = 'iron_shield',
  type = 'armor',
  stat = 'def',
  effect = 3
}
