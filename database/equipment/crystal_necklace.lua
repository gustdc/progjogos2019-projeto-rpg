
return {
  name = 'Crystal Necklace',
  appearance = 'crystal_necklace',
  type = 'artifact',
  stat = 'res',
  effect = 5
}
