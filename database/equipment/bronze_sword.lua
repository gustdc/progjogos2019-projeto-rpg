
return {
  name = 'Bronze Sword',
  appearance = 'bronze_sword',
  type = 'weapon',
  stat = 'str',
  effect = 2
}
