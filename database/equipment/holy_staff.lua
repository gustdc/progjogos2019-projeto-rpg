
return {
  name = 'Holy Staff',
  appearance = 'holy_staff',
  type = 'weapon',
  stat = 'str',
  effect = 2
}
