
return {
  name = 'Long Bow',
  appearance = 'long_bow',
  type = 'weapon',
  stat = 'str',
  effect = 2
}
