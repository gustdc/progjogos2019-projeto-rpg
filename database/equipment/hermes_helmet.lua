
return {
  name = "Herme's Helmet",
  appearance = 'hermes_helmet',
  type = 'armor',
  stat = 'def',
  effect = 3
}
