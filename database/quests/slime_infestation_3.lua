
return {
  title = 'Slime Infestation 3',
  party = { 'warrior', 'archer', 'priest' },
  equipment = { 'crystal_necklace', 'feather_boots', 'angelhair' },
  backpack = {
    {'strength_potion', 5},
  },
  encounters = {
    { 'green_slime', 'green_slime', 'blue_slime' },
    { 'blue_slime', 'red_slime' },
    { 'blue_slime', 'red_slime', 'blue_slime' },
  }
}

