
return {
  title = 'Slime Infestation 2',
  party = { 'warrior', 'archer', 'priest' },
  equipment = { 'iron_shield', 'hermes_helmet', 'popes_hat' },
  backpack = {
    {'health_potion', 5},
    {'defense_potion', 5},
  },
  encounters = {
    { 'green_slime', 'green_slime', 'green_slime', 'green_slime' },
    { 'blue_slime', 'blue_slime', 'blue_slime' },
    { 'red_slime', 'red_slime' },
  }
}

