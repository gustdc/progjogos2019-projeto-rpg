
return {
  title = 'Slime Infestation',
  party = { 'warrior', 'archer', 'priest' },
  equipment = { 'bronze_sword', 'long_bow', 'holy_staff' },
  backpack = {
    {'health_potion', 1},
    {'strength_potion', 1},
    {'defense_potion', 1},
  },
  encounters = {
    { 'green_slime' },
    { 'green_slime', 'green_slime', 'green_slime' },
    { 'blue_slime', 'red_slime' },
    { 'blue_slime', 'blue_slime', 'blue_slime' },
  }
}

