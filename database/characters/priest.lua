
return {
  name = "Friendly Priest",
  appearance = 'priest',
  max_hp = 10,
  max_resource = 35,
  str = 7,
  def = 3,
  speed = 3,
  max_equipment = 2,
  can_equip = {
    'holy_staff',
    'popes_hat',
    'angelhair'
  },
  skills = {
    {
      name = "Inner Strength",
      type = "heal",
      damage_multiplier = 1.15,
      resource_cost = 5,
      number_of_targets = 1
    },
    {
      name = "Heaven's Blessing",
      type = "heal",
      damage_multiplier = 0.65,
      resource_cost = 15,
      number_of_targets = 0
    }
  }
}

