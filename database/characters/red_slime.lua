
return {
  name = "Red Slime",
  appearance = 'red_slime',
  max_hp = 12,
  str = 8,
  def = 2,
  speed = 1,
  behavior = "attack_lowest_hp"
}

