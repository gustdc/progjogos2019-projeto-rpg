
return {
  name = "Veteran Warrior",
  appearance = 'knight',
  max_hp = 20,
  max_resource = 20,
  str = 4,
  def = 6,
  speed = 4,
  max_equipment = 1,
  can_equip = {
    'bronze_sword',
    'iron_shield',
    'crystal_necklace'
  },
  skills = {
    {
      name = "Shield Bash",
      type = "damage_amplifier",
      damage_multiplier = 1.15,
      resource_cost = 5,
      number_of_targets = 1
    },
    {
      name = "Cleave",
      type = "damage_amplifier",
      damage_multiplier = 0.65,
      resource_cost = 10,
      number_of_targets = 0
    }
  }
}

