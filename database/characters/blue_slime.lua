
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 8,
  str = 6,
  def = 6,
  speed = 1,
  behavior = "attack_strongest"
}

