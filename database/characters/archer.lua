
return {
    name = "Recluse Archer",
    appearance = 'archer',
    max_hp = 12,
    max_resource = 20,
    str = 5,
    def = 5,
    speed = 5,
    max_equipment = 2,
    can_equip = {
      'long_bow',
      'hermes_helmet',
      'feather_boots'
    },
    skills = {
        {
            name = "Take Aim",
            type = "damage_amplifier",
            damage_multiplier = 1.25,
            resource_cost = 5,
            number_of_targets = 1
        },
        {
            name = "Rain of Arrows",
            type = "damage_amplifier",
            damage_multiplier = 0.45,
            resource_cost = 10,
            number_of_targets = 0
        }
    }
}

