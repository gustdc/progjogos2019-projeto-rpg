
return {
    name = "Recluse Archer",
    appearance = 'archer',
    max_hp = 12,
    max_resource = 20,
    str = 5,
    def = 5,
    skills = {
        {
            name = "Take Aim",
            type = "damage_amplifier",
            damage_multiplier = 1.25,
            resource_cost = 5,
            number_of_targets = 2
        }
    }
}

