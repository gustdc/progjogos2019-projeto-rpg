
return {
  name = "Friendly Priest",
  appearance = 'priest',
  max_hp = 10,
  max_resource = 20,
  str = 7,
  def = 3
}

