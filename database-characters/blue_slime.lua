
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 12,
  str = 6,
  def = 6
}

