local Character = require 'common.class' ()
local Skill = require 'model.skill'

function Character:_init(spec)
  self.spec = spec
  self.name = spec.name
  self.appearance = spec.appearance
  self.hp = spec.max_hp
  self.str = spec.str
  self.def = spec.def
  self.speed = spec.speed
  self.res = spec.max_resource or 0
  self.equipment = {}
  self.nequipment = 1
  self.max_equipment = spec.max_equipment or 0
  self.equippables = spec.can_equip
  self.behavior = spec.behavior
  self.skills = {}

  if spec.skills then
    for i, skill in pairs(spec.skills) do
      self.skills[i] = Skill(skill)
    end
  end
end

function Character:get_name()
  return self.name
end

function Character:is_dead()
  return self.hp == 0
end

function Character:has_skills()
  return self.spec.skills and true
end

function Character:get_appearance()
  return self.appearance
end

function Character:get_hp()
  return self.hp, self.spec.max_hp
end

function Character:get_res()
  return self.res, (self.spec.max_resource or 0)
end

function Character:get_equipment()
  return self.equipment, (self.max_equipment or 0)
end

function Character:inc_hp(effect)
  local hp = self.hp + effect
  self.hp = math.min(hp, self.spec.max_hp)
  if hp >= self.spec.max_hp then
    return 0
  else
    return effect
  end
end

function Character:inc_str(effect)
  self.str = self.str + effect
  return effect
end

function Character:inc_def(effect)
  self.def = self.def + effect
  return effect
end

function Character:inc_speed(effect)
  self.speed = self.speed + effect
  return self.speed
end

function Character:inc_res(effect)
  self.spec.max_resource = self.spec.max_resource + effect
  self.res = self.spec.max_resource
end

function Character:can_equip(equipment)
  for _, equip_name in ipairs(self.equippables) do
    if equip_name == equipment.appearance then
      return true
    end
  end

  return false
end

function Character:has_equip_type(equipment)
  for _, equip in ipairs(self.equipment) do
    if equip.type == equipment.type then
      return true
    end
  end

  return false
end

function Character:equip(equipment)
  if self.nequipment <= self.max_equipment then
    local message

    if not self:can_equip(equipment) then
      message = "%s can't equip '%s'"
      return message:format(self.name, equipment.name)
    end

    if self:has_equip_type(equipment) then
      local type_message = "%s is already using an equipment of type '%s'"
      return type_message:format(self.name, equipment.type)
    end

    self.equipment[self.nequipment] = equipment
    local increase_stat = self['inc_' .. equipment:get_stat()]
    increase_stat(self, equipment:get_effect())
    self.nequipment = self.nequipment + 1
    return ""
  else
    return "Character is using max capacity!"
  end
end

function Character:attack(target)
  local damage = self.str
  local before = target:get_hp()
  target:take_damage(damage)
  return before - target:get_hp()
end

-- https://rpg.fandom.com/wiki/Damage_Formula
function Character:take_damage(damage)
  damage  = math.max(0, damage * (100 / (100 + self.def)))
  self.hp = math.max(0, math.floor(self.hp - damage))
end

return Character
