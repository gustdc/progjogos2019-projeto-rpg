local Skill = require 'common.class' ()

local FRIENDLY_SKILLS = {heal = true}

function Skill:_init(spec)
  self.spec = spec
  self.name = spec.name
  self.type = spec.type
  self.number_of_targets = spec.number_of_targets
  self.dmg_mul = spec.damage_multiplier
  self.cost = spec.resource_cost
  self.skill_handlers = {
    damage_amplifier = self.damage_amplifier_skill,
    heal = self.heal_skill
  }
end

function Skill:get_name()
    return self.name
end

function Skill:is_friendly()
    return FRIENDLY_SKILLS[self.type] or false
end

function Skill:use_skill(caster, targets)
    local curr_res = caster:get_res()
    if curr_res >= self.cost then
      caster.res = curr_res - self.cost

      local skill_handler = self.skill_handlers[self.type]
      if skill_handler then
        return skill_handler(self, caster, targets)
      end
    else
      return "Couldn't cast skill. No res left!"
    end
end

function Skill:damage_amplifier_skill(caster, targets)
  local damage_dealt = {}
  for _,target in ipairs(targets) do
    local damage = caster.str * self.dmg_mul
    local before = target:get_hp()
    target:take_damage(damage)
    table.insert(damage_dealt, before - target:get_hp())
  end

  return damage_dealt
end

function Skill:heal_skill(caster, targets)
  local healing_done = {}
  for _, target in ipairs(targets) do
    local heal_value = caster.str * self.dmg_mul
    local hp, max_hp = target:get_hp()
    target.hp = math.min(max_hp, hp + heal_value)
    table.insert(healing_done, target:get_hp() - hp)
  end

  return healing_done
end

return Skill
