
local Equipment = require 'common.class' ()

function Equipment:_init(spec)
  self.name = spec.name
  self.appearance = spec.appearance
  self.type = spec.type
  self.stat = spec.stat
  self.effect = spec.effect
end

function Equipment:get_name()
  return self.name
end

function Equipment:get_stat()
  return self.stat
end

function Equipment:get_effect()
  return self.effect
end

return Equipment
