local Item = require 'common.class' ()

function Item:_init(spec, quantity)
  self.name = spec.name
  self.appearance = spec.appearance
  self.stat = spec.stat
  self.effect = spec.effect
  self.quantity = quantity
end

function Item:get_name()
  return self.name
end

function Item:get_stat()
  return self.stat
end

function Item:get_effect()
  return self.effect
end

function Item:get_quantity()
  return self.quantity
end

function Item:use_item(character)
  local increase_stat = character['inc_' .. self.stat]
  local effect = increase_stat(character, self.effect)
  self.quantity = self.quantity - 1
  return effect, self.quantity
end

return Item
